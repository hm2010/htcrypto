//Vigenere.h
#pragma once
#ifndef VIGENERE_H
#define VIGENERE_H

#include "Cipher.h"
#define MAX_VALUE 256

class Vigenere: public Cipher{
private:
	void encrypt(uint16_t*);
	void decrypt(uint16_t*);
	
public:
	Vigenere(uint16_t*);
	~Vigenere(void);
};

#endif