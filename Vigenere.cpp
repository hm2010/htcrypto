#include "Vigenere.h"

Vigenere::Vigenere(uint32_t _key): Cipher(_key){}
Vigenere::~Vigenere(void){}

void Vigenere::encrypt(uint16_t* block){
	for (uint8_t i = 0; i < block_size; i++){
		block[i] = (block[i] + session_key[i]) % MAX_VALUE;
	}
}
void Vigenere::decrypt(uint16_t* block){
	for (uint8_t i = 0; i < block_size; i++){
		block[i] = (block[i] - session_key[i] + MAX_VALUE) % MAX_VALUE;
	}
}