#include "Encryption_mode.h"

void ECB(void){
	if (Cipher::mode == "encrypt"){
		Cipher::curr_block = encrypt(Cipher::curr_block, Cipher::session_key);
	}
	else{
		Cipher::curr_block = decrypt(Cipher::curr_block, Cipher::session_key);
	}
	
}

void CBC(void){
	if (Cipher::mode == "encrypt"){
		Cipher::curr_block = encrypt(Cipher::prev_block xor Cipher::curr_block, Cipher::session_key);
	}
	else{
		Cipher::
	}
}

void CFB(void){
	if (Cipher::mode == "encrypt"){
		Cipher::curr_block = encrypt(Cipher::prev_block, Cipher::session_key) xor Cipher::curr_block;
	}
	else{
		Cipher::curr_block = decrypt(Cipher::prev_block, Cipher::session_key) xor Cipher::curr_block;
	}
}

void OFB(void){
	if (Cipher::mode == "encrypt"){
		Cipher::temp_block = encrypt(Cipher::temp_block, Cipher::session_key);
	}
	else{
		Cipher::temp_block = decrypt(Cipher::temp_block, Cipher::session_key);
	}
	Cipher::curr_block = Cipher::temp_block xor Cipher::curr_block;
	
}