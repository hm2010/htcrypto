//Сipher.h - abstract class of ciphers
#pragma once
#ifndef CIPHER_H
#define CIPHER_H

#include <fstream>
#include <cstdint>
#include <cstdlib>
#include <string>

#define BLOCK_SIZE_DEFAULT 4
#include "Encryption_mode.h"
#include "Padding_mode.h"


class Cipher{
	uint8_t block_size = BLOCK_SIZE_DEFAULT;
	uint32_t key;
	uint16_t session_key[block_size];
	uint16_t IV[block_size];

	uint16_t temp_block[block_size];
	uint16_t curr_block[block_size];
	uint16_t prev_block[block_size];
	virtual void session_key_init (void);
	virtual void encrypt(uint16_t*) = 0;			//current block
	virtual void decrypt(uint16_t*) = 0;

	std::string mode;								//encrypt or decrypt
public:
	Cipher(uint32_t);
	virtual ~Cipher(void){}
	
	std::ofstream& crypto(std::ifstream&);
	
	/* from Encryption_mode.h */
	friend void ECB(void);
	friend void CBC(void);
	friend void CFB(void);
	friend void OFB(void);
	
	/* from Padding_mode.h */
	friend void PKCS7(uint16_t*);
	friend void ANSI_X923(uint16_t*);
	friend void ISO_10126(uint16_t*);
};

#endif