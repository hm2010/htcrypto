//Encryption_mode.h
#pragma once
#ifndef ENCRYPTION_MODE_H
#define ENCRYPTION_MODE_H

void ECB(uint16_t*);				//Electronic Codebook
void CBC(uint16_t*, uint16_t*);		//Cipher Block Chaining
void CFB(uint16_t*, uint16_t*);		//Cipher Feedback
void OFB(uint16_t*);				//Output Feedback

#endif