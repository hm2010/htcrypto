#include "Cipher.h"

virtual void Cipher::session_key_init (void){
	srand(key);
	for (uint8_t i = 0; i < block_size; i++){
		session_key[i] = rand() % sizeof(uint8_t);
	}
}
Cipher::Cipher(uint32_t _key){
	key = _key;
	session_key_init();
}
std::ofstream& crypto(std::ifstream& file_in, string _mode, string enc_mode, string pad_mode){

	mode = _mode;
	prev_block = IV;
	while (!file_in.eof()){
		file_in.getline(curr_block, block_size);
		if (enc_mode == "ECB"){
			ECB(void);
		}
		else if (enc_mode == "CBC"){
			CBC(void);
		}
		else if(enc_mode == "CFB"){
			CFB(void);
		}
		else if(enc_mode == "OFB"){
			OFB(void);
		}
	}

}
void ECB(void);
void CBC(void);
void CFB(void);
void OFB(void);

void PKCS7(uint16_t*); 
void ANSI_X923(uint16_t*);
void ISO_10126(uint16_t*);